

/*-------------------------toggle navbar----------------------------------*/ /*coded by Sandeep Rasali*/
const navToggler = document.querySelector(".nav-toggler");
navToggler.addEventListener("click", toggleNav);

function toggleNav(){
    navToggler.classList.toggle("active");
    document.querySelector(".nav").classList.toggle("open");
}

/*---------------------when clicking on the nav-item, it closes the navigation bar-------------------*/ /*coded by Sandeep Rasali*/

document.addEventListener("click", function(e){
    if(e.target.closest(".nav-item")){
        toggleNav();
    }
});

/*---------------------sticky header --------------------------*/ /*coded by Sandeep Rasali*/  /*additional feature in last MVP*/
window.addEventListener("scroll", function(){
    if(this.pageYOffset > 60){
        document.querySelector(".header").classList.add("sticky");
    }
    else{
        document.querySelector(".header").classList.remove("sticky");
    }
});



/*----------------------JS for Google Maps-------------------------*/

/*Coded by Sandeep Rasali (S342931) on May 10, 2021 at 2:35, Darwin time*/

/*This displays a marker at darwin of Australia where our domain called as "Amigos"
restaurant is located*/
/*When the user clicks the marker, an info window opens depicting some basic information 
about our restaurant */

function initMap() {
    const darwin = { lat: -12.4629, lng: 130.8449 };
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: darwin,
    });
    const contentString = "<p><b>Amigos</b><br> The one and only Authentic Mexican Food in the town.</p>"
    const infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 200,
    });
    const marker = new google.maps.Marker({
      position: darwin,
      map,
      title: "Darwin (Amigos)",
    });
    marker.addListener("click", () => {
      infowindow.open(map, marker);
    });
  };


